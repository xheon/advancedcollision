#ifndef ADVANCEDCOLLISION_KDTREE_H
#define ADVANCEDCOLLISION_KDTREE_H

#include <vector>
#include "DirectXMath.h"
#include "Ball.h"
#include <array>
#include <set>
#include <map>

using namespace DirectX;

namespace AdvancedCollision
{
	struct Node
	{
		bool isLeaf = false;

		int childLeft = -1;
		int childRight = -1;

		std::vector<int> ballIds;
	};

	class KdTree
	{
	public:
		void BuildKdTree(std::vector<Ball>& balls, float width, float height, float depth);
		KdTree(std::vector<Ball>& balls);
		KdTree();

		int insertId = 0;

		std::vector<Node> nodes;

		//std::array<bool,  checkedPairs;
		std::vector<Node> leaves;

	private:
		void Split(std::vector<Ball>& balls, int parentId, const int axis, std::array<float, 3> start, std::array<float, 3> end, int currentDepth);
	};
}


#endif /* ADVANCEDCOLLISION_KDTREE_H */