#include "KdTree.h"
#include <algorithm>
#include <array>
#include <vector>

namespace AdvancedCollision
{
	KdTree::KdTree()
	{
	}
	
	KdTree::KdTree(std::vector<Ball>& balls)
	{
		nodes.reserve(2048);

		Node root;

		if (nodes.size() > 0)
		{
			nodes[0] = root;
		}
		else
		{
			nodes.emplace_back(root);
		}
		insertId++;

		for (int i = 0; i < balls.size(); ++i)
		{
			nodes[0].ballIds.push_back(i);
		}

		if (balls.size() < 4) {
			nodes[0].isLeaf = true;
			return;
		}
		else {
			nodes[0].isLeaf = false;
		}

		//checkedPairs.
	}
	
	void KdTree::Split(std::vector<Ball>& balls, int parentId, const int axis, std::array<float, 3> start, std::array<float, 3> end, int currentDepth) {
		Node l;

		if (nodes.size() > insertId)
		{
			nodes[insertId] = l;
			nodes[parentId].childLeft = insertId;
		}
		else
		{
			nodes.emplace_back(l);
			nodes[parentId].childLeft = nodes.size() - 1;
		}
		insertId++;

		
		Node r;

		if (nodes.size() > insertId)
		{
			nodes[insertId] = r;
			nodes[parentId].childRight = insertId;
		}
		else
		{
			nodes.emplace_back(r);
			nodes[parentId].childRight = nodes.size() - 1;
		}
		insertId++;


		const float pivot = ((end[axis] - start[axis]) / 2.0f) + start[axis];

		for ( auto ballId : nodes[parentId].ballIds )
		{
			switch (axis)
			{
			case 0: {
				if (XMVectorGetX(balls[ballId].position_) - 0.5f < pivot) {
					nodes[nodes[parentId].childLeft].ballIds.emplace_back( ballId );
				}

				if ( XMVectorGetX( balls[ballId].position_ ) + 0.5f >= pivot )
				{
					nodes[nodes[parentId].childRight].ballIds.emplace_back( ballId );
				}
				break;
			}

			case 1: {
				if ( XMVectorGetY( balls[ballId].position_ ) - 0.5f < pivot )
				{
					nodes[nodes[parentId].childLeft].ballIds.emplace_back( ballId );
				}

				if ( XMVectorGetY( balls[ballId].position_ ) + 0.5f >= pivot )
				{
					nodes[nodes[parentId].childRight].ballIds.emplace_back( ballId );
				}
				break;
			}

			case 2: {
				if ( XMVectorGetZ( balls[ballId].position_ ) - 0.5f < pivot )
				{
					nodes[nodes[parentId].childLeft].ballIds.emplace_back( ballId );
				}

				if ( XMVectorGetZ( balls[ballId].position_ ) + 0.5f >= pivot )
				{
					nodes[nodes[parentId].childRight].ballIds.emplace_back( ballId );
				}
				break;
			}

			}
		}
			
		if ( nodes[nodes[parentId].childLeft].ballIds.size() < 16 || currentDepth > 14 )
		{
			nodes[nodes[parentId].childLeft].isLeaf = true;
			leaves.push_back( nodes[nodes[parentId].childLeft] );

		}
		else {
			std::array<float, 3> leftEnd = end;
			leftEnd[axis] = pivot;
			Split(balls, nodes[parentId].childLeft, (axis + 1) % 3, start, leftEnd, ++currentDepth);
		}
		if ( nodes[nodes[parentId].childRight].ballIds.size() < 16 || currentDepth > 14 )
		{
			nodes[nodes[parentId].childRight].isLeaf = true;
			leaves.push_back( nodes[nodes[parentId].childRight] );
		}
		else {
			std::array<float, 3> rightStart = start;
			rightStart[axis] = pivot;
			Split(balls, nodes[parentId].childRight, (axis + 1) % 3, rightStart, end, ++currentDepth);
		}
	}

	void KdTree::BuildKdTree(std::vector<Ball>& balls, float width, float height, float depth) {
		std::array<float, 3> start = { -width / 2, -height / 2, -depth / 2 };
		std::array<float, 3> end = { width / 2, height / 2, depth / 2 };

		Split(balls, 0, 0, start, end, 0);
	}
}