#ifndef ADVANCEDCOLLISION_SIMULATION_H
#define ADVANCEDCOLLISION_SIMULATION_H

#include <vector>
#include "DirectXMath.h"
#include "Ball.h"
#include "KdTree.h"
#include "Grid.h"
#include "timer.h"

using namespace DirectX;

namespace AdvancedCollision
{
	enum E_STORAGE
	{
		NAIVE,
		GRID,
		KDTREE
	};
	
	class Simulation
	{
	public:
		std::vector<Ball> balls_;
		XMVECTOR position_;
		float size_;
		int countBallsX_;
		int countBallsY_;
		int countBallsZ_;
		int width_ = 0;
		E_STORAGE storage_;
		KdTree tree_;
		Grid grid_;
		MuTime myTimer;

		std::vector<bool> checkedPairs;
		std::vector<int> pairs;

		Simulation( XMVECTOR position, float size_, int ballsX, int ballsY, int ballsZ, E_STORAGE storage);
		~Simulation();

		void CleanUp();
		void Initialize( ID3D11DeviceContext* pd3dImmediateContext );
		void Update(float fElapsedTime, int axis = -1, float force = -9.81f);
		void BoundingBoxCheck( XMVECTOR& position, XMVECTOR& velocity );
		void CollisionDetection();
		void CollisionResponse(Ball* p1, Ball* p2, XMVECTOR distance);
		void Check(  Node leaf );
		void TravelTree(int nodeId );
		int IDX(int i, int j);
	};
}

#endif /* ADVANCEDCOLLISION_SIMULATION_H */