#ifndef ADVANCEDCOLLISION_BALL_H
#define ADVANCEDCOLLISION_BALL_H

#include <DirectXMath.h>
#include "GeometricPrimitive.h"

using namespace DirectX;

namespace AdvancedCollision
{
	class Ball
	{
	public:
		float mass_ = 1.0f;
		float radius_ = 0.5f;

		XMVECTOR position_;
		XMVECTOR velocity_;
		XMVECTOR force_;

		GeometricPrimitive* pSphere_;

		Ball( XMVECTOR position, XMVECTOR velocity, XMVECTOR force, GeometricPrimitive* pSphere );
		~Ball();
		void Update( float fElapsedTime );
		void ClearForce();
		void Destroy();

		Ball& operator= (Ball a);
	};
}

#endif /* ADVANCEDCOLLISION_BALL_H */