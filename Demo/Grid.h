#ifndef ADVANCEDCOLLISION_GRID_H
#define ADVANCEDCOLLISION_GRID_H

#include "DirectXMath.h"
#include "Ball.h"
#include <vector>
#include <set>
#include <map>

using namespace DirectX;

namespace AdvancedCollision
{
	class Grid {
	public:
		void SortBalls( std::vector<Ball>& balls, float width, float height, float depth );

		std::map<int, std::vector<int>> cells_;

		const int h = 2;
		float width_ = 0;
		float height_ = 0;
		float depth_ = 0;

		int widthSize_ = 0;
		int heightSize_ = 0;
		int depthSize_ = 0;	
	};
}

#endif /* ADVANCEDCOLLISION_GRID_H */