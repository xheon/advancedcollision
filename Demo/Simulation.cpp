#include "time.h"
#include <algorithm>
#include "KdTree.h"
#include <iostream>
#include <set>
#include "timer.h"
#include "Simulation.h"

namespace AdvancedCollision
{
	Simulation::Simulation( XMVECTOR position, float size, int ballsX, int ballsY, int ballsZ, E_STORAGE storage ) :
		position_( position ),
		size_( size ),
		countBallsX_( ballsX ),
		countBallsY_( ballsY ),
		countBallsZ_( ballsZ ),
		storage_( storage ),
		balls_()

	{
		//printf( "Balls: %i\n", balls_.size() );
	}

	Simulation::~Simulation()
	{
		CleanUp();
	}

	void Simulation::Initialize( ID3D11DeviceContext* pd3dImmediateContext )
	{
		srand( static_cast<int> (time( NULL )) );

		// Height
		for ( int i = 0; i < countBallsY_; ++i )
		{
			// Width
			for (int j = 0; j < countBallsX_; ++j)
			{
				// Depth
				for (int k = 0; k < countBallsZ_ ; ++k)
				{
					std::unique_ptr<GeometricPrimitive> pSphere = GeometricPrimitive::CreateSphere(pd3dImmediateContext);
					//Ball ball( XMVectorSet( 1, 1 + i, 0, 0 ), XMVectorZero(), XMVectorZero(), pSphere.release() );

					//balls_.push_back( &ball );
					balls_.emplace_back(XMVectorSet(1 + j - size_/2, -size_ / 2 + i * 1.01f + 0.6f , 1 + k - size_/2, 0), XMVectorZero() , XMVectorZero(), pSphere.release()); //XMVectorSet(rand() % 3, 0, rand() % 3, 0)
				}
			}
		}

		std::unique_ptr<GeometricPrimitive> pSphere = GeometricPrimitive::CreateSphere( pd3dImmediateContext );
		balls_.emplace_back( XMVectorSet( 1.5f - size_ / 2, 9 - size_ / 2, 1.5f - size_ / 2, 0 ), XMVectorZero(), XMVectorZero(), pSphere.release() ); //XMVectorSet(rand() % 3, 0, rand() % 3, 0)

		//pSphere = GeometricPrimitive::CreateSphere( pd3dImmediateContext );
		//balls_.emplace_back( XMVectorSet( -8, -8, 3, 0 ), XMVectorSet(0,4,0,0), XMVectorZero(), pSphere.release() );

		/*pSphere = GeometricPrimitive::CreateSphere( pd3dImmediateContext );
		balls_.emplace_back( XMVectorSet( -8, 8, 8, 0 ), XMVectorZero(), XMVectorZero(), pSphere.release() );

		pSphere = GeometricPrimitive::CreateSphere( pd3dImmediateContext );
		balls_.emplace_back( XMVectorSet( 8, -8, 0, 0 ), XMVectorZero(), XMVectorZero(), pSphere.release() );

		pSphere = GeometricPrimitive::CreateSphere( pd3dImmediateContext );
		balls_.emplace_back( XMVectorSet( 8, 9, -4, 0 ), XMVectorZero(), XMVectorZero(), pSphere.release() );

		pSphere = GeometricPrimitive::CreateSphere( pd3dImmediateContext );
		balls_.emplace_back( XMVectorSet( 8, 6, -4, 0 ), XMVectorZero(), XMVectorZero(), pSphere.release() );*/

		/*pSphere = GeometricPrimitive::CreateSphere( pd3dImmediateContext );
		balls_.emplace_back( XMVectorSet( 0, 0, -8, 0 ), XMVectorZero(), XMVectorZero(), pSphere.release() );*/

		//pSphere = GeometricPrimitive::CreateSphere( pd3dImmediateContext );
		//balls_.emplace_back( XMVectorSet (5, 0, 0, 0 ), XMVectorZero(), XMVectorZero(), pSphere.release() );

		if ( storage_ == KDTREE )
		{

			tree_ = KdTree( balls_ );

			width_ = balls_.size();
			checkedPairs.resize(width_*width_, false);
		}
		if (storage_ == GRID)
		{
			width_ = balls_.size();
			checkedPairs.resize(width_*width_, false);
		}
	}

	void Simulation::CleanUp()
	{
		for ( auto& it : balls_ )
		{
			it.Destroy();
		}
	}

	void Simulation::Update(float fElapsedTime, int axis, float force)
	{
		CollisionDetection();
		
		for ( auto& it : balls_ )
		{
			BoundingBoxCheck( it.position_, it.velocity_ );
			
			if ( XMVectorGetY(it.force_) == 0)
			{
				it.force_ = XMVectorSetY (it.force_, -9.81f);
			}

			switch (axis) {
			case 0:
			{
				it.force_ = XMVectorSetX(it.force_, force);
				break;
			}
			case 1:
			{
				it.force_ = XMVectorSetY(it.force_, force);
				break;
			}
			case 2:
			{
				it.force_ = XMVectorSetZ(it.force_, force);
				break;
			}
			default:
				break;
			}
			it.Update( fElapsedTime );
			it.ClearForce();
		}
	}


	void Simulation::BoundingBoxCheck( XMVECTOR& position, XMVECTOR& velocity )
	{
		float distance;

		distance = XMVectorGetX( position ) - size_ / 2;
		if ( fabs( distance ) <= 0.5f )
		{
			velocity = XMVectorSetX( velocity, -XMVectorGetX( velocity ) );
		}

		distance = XMVectorGetX( position ) + size_ / 2;
		if ( fabs( distance ) <= 0.5f )
		{
			velocity = XMVectorSetX( velocity, -XMVectorGetX( velocity ) );
		}

		distance = XMVectorGetY( position ) - size_ / 2;
		if ( fabs( distance ) <= 0.5f )
		{
			velocity = XMVectorSetY( velocity, -XMVectorGetY( velocity ) );
		}

		distance = XMVectorGetY( position ) + size_ / 2;
		if ( fabs( distance ) <= 0.5f )
		{
			

			if (XMVectorGetY(velocity)  > 0.1f)
			{
				XMVECTOR ground = position;
				ground = XMVectorSetY( ground, -size_ / 2 );

				XMVECTOR dist = position - ground; 
				float lambda = 25.0f;
				float force = lambda * (1.0f - XMVectorGetX( XMVector3Length( dist ) ));

				XMVECTOR forceVector = force * XMVector3Normalize( dist );

				//printf( "- Ground: %f\n", force );
				//XMVectorGetX(p1->force_), XMVectorGetY(p1->force_), XMVectorGetZ(p1->force_),

				position = XMVectorSetY( position, 0.5f - size_ / 2 );
				velocity = forceVector;
				//p2->force_ = -forceVector;
				
				//position = XMVectorSetY(position, 0.5f - size_/2);
				//velocity = XMVectorSetY(velocity, 0);
			}
			else
			{
				position = XMVectorSetY( position, 0.5f - size_ / 2 );
				velocity = XMVectorSetY(velocity, 0);
			}
		}

		distance = XMVectorGetZ( position ) - size_ / 2;
		if ( fabs( distance ) <= 0.5f )
		{
			velocity = XMVectorSetZ( velocity, -XMVectorGetZ( velocity ) );
		}

		distance = XMVectorGetZ( position ) + size_ / 2;
		if ( fabs( distance ) <= 0.5f )
		{
			velocity = XMVectorSetZ( velocity, -XMVectorGetZ( velocity ) );
		}

		return;
	}

	void Simulation::Check( Node leaf )
	{
		
		/*if ( leaf.ballIds.size() <= 0 )
		{
			return;
		}
		for ( auto i = leaf.ballIds.begin(); i != leaf.ballIds.end() - 1; ++i )
		{
			for ( auto j = i + 1; j < leaf.ballIds.end(); ++j )
			{
				if ( tree_.checkedPairs.find( std::pair<int, int>( *i, *j ) ) == tree_.checkedPairs.end() )
				{
					XMVECTOR distance = balls_[*i].position_ - balls_[*j].position_;


					if ( XMVectorGetX( XMVector3Length( distance ) ) < 1 )
					{
						//printf( " Pair: %i - %i: ", *i, *j );

						CollisionResponse( &balls_[*i], &balls_[*j], distance );
					}
					tree_.checkedPairs.insert( std::pair<int, int>( *i, *j ) );
				}
			}
		}*/
	}

	void Simulation::TravelTree( int nodeId )
	{
		/*if ( tree_.nodes[nodeId].isLeaf )
		{
			Check( tree_.nodes[nodeId] );
		}
		else
		{
			TravelTree( tree_.nodes[nodeId].childLeft );
			TravelTree( tree_.nodes[nodeId].childRight );
		}*/
	}

	int Simulation::IDX(int i, int j) {
		if (i < j) {
			return i * width_ + j;
		}
		else {
			return j * width_ + i;
		}
	}

	void Simulation::CollisionDetection()
	{
		
		if (storage_ == NAIVE)
		{
			myTimer.get();
			// Test each balls against each other
			int i = 0;
			for (auto it = balls_.begin(); it != balls_.end() - 1; ++it)
			{
				int j = i + 1;
				for (auto jt = it + 1; jt != balls_.end(); ++jt)
				{
					XMVECTOR distance = it->position_ - jt->position_;

					if (XMVectorGetX(XMVector3Length(distance)) < 1)
					{
						//printf( " Pair: %i - %i: ", i,j  );
						CollisionResponse(it._Ptr, jt._Ptr, distance);
					}
					j++;
				}
				i++;
			}
			//std::cout << "\tCollision time: " << myTimer.update().time << std::endl;

		}
		else if (storage_ == KDTREE) {
			//KdTree tree;

			//myTimer.get();
			//tree_.nodes.clear();
			for (auto& i : pairs) {
				checkedPairs[i] = false;
			}
			pairs.clear();
			tree_.leaves.clear();
			tree_.insertId = 1;
			//std::cout << "\n\tClear time: " << myTimer.update().time << std::endl;
			
			//myTimer.get();
			tree_.BuildKdTree( balls_, size_, size_, size_ );
			//balls_ = tree.nodes[0].balls;
			//std::cout << "\tBuild time: " << myTimer.update().time << std::endl;

			//myTimer.get();
			// Check for collisions
			/*if ( tree_.nodes[0].isLeaf )
			{
				Check( 0);
			}
			else
			{
				TravelTree( tree_.nodes[0].childLeft );
				TravelTree( tree_.nodes[0].childRight );
			}*/

			for ( auto& it : tree_.leaves )
			{
				if ( it.ballIds.size() > 1 )
				{

					for ( auto i = it.ballIds.begin(); i != it.ballIds.end() - 1; ++i )
					{
						for ( auto j = i + 1; j < it.ballIds.end(); ++j )
						{
							int index = IDX(*i, *j);
							if ( !checkedPairs [index])
							{
								XMVECTOR distance = balls_[*i].position_ - balls_[*j].position_;

								

								if ( XMVectorGetX( XMVector3Length( distance ) ) < 1 )
								{
									//printf( " Pair: %i - %i: ", *i, *j );

									CollisionResponse( &balls_[*i], &balls_[*j], distance );								
								}

								//if ( std::find( tree_.checkedPairs.begin(), tree_.checkedPairs.end(), smaller ) == tree_.checkedPairs.end())
								//{
								checkedPairs[index] = true;
								pairs.push_back(index);
							}
						}
					}
				}
			}
			//std::cout << "\tCollision time: " << myTimer.update().time << std::endl;

		}
		else if (storage_ == GRID) {
			//myTimer.get();
			for (auto& i : pairs) {
				checkedPairs[i] = false;
			}
			pairs.clear();
			grid_.cells_.clear();
			//std::cout << "\n\tClear time: " << myTimer.update().time << std::endl;

			//myTimer.get();

			grid_.SortBalls( balls_, size_, size_, size_ );
			//std::cout << "\tBuild time: " << myTimer.update().time << std::endl;

			myTimer.get();

			for ( auto pair : grid_.cells_ )
			{
				int index = pair.first;

				int zIndex = index % grid_.depthSize_;
				int yIndex = ((index - zIndex) / grid_.depthSize_) % grid_.heightSize_;
				int xIndex = (index - zIndex - yIndex) / (grid_.depthSize_ * grid_.heightSize_);

				bool	xPosBorder = false, 
						xNegBorder = false, 
						yPosBorder = false, 
						yNegBorder = false, 
						zPosBorder = false, 
						zNegBorder = false;

				if ( index < (grid_.depthSize_ * grid_.heightSize_) )
				{
					xNegBorder = true;
				}

				if ( index >= (grid_.cells_.size() - grid_.depthSize_ * grid_.heightSize_) )
				{
					xPosBorder = true;
				}

				if ( index % grid_.depthSize_ == 0 )
				{
					zNegBorder = true;
				}

				if ( index % grid_.depthSize_ == grid_.depthSize_ - 1 )
				{
					zPosBorder = true;
				}

				if ( index % (grid_.depthSize_ * grid_.heightSize_) < grid_.depthSize_ )
				{
					yNegBorder = true;
				}

				if ( index % (grid_.depthSize_ * grid_.heightSize_) >= (grid_.depthSize_ * grid_.heightSize_) - grid_.depthSize_ )
				{
					yPosBorder = true;
				}

				int startX, endX, startY, endY, startZ, endZ;
				
				// Define search range
				if ( xNegBorder )
				{
					startX = xIndex;
				}
				else
				{
					startX = xIndex - 1;
				}

				if ( xPosBorder )
				{
					endX = xIndex;
				}
				else
				{
					endX = xIndex + 1;
				}

				if ( yNegBorder )
				{
					startY = yIndex;
				}
				else
				{
					startY = yIndex - 1;
				}

				if ( yPosBorder )
				{
					endY = yIndex;
				}
				else
				{
					endY = yIndex + 1;
				}

				if ( zNegBorder )
				{
					startZ = zIndex;
				}
				else
				{
					startZ = zIndex - 1;
				}

				if ( zPosBorder )
				{
					endZ = zIndex;
				}
				else
				{
					endZ = zIndex + 1;
				}

				for ( int x = startX; x <= endX; x = x + 1 )
				{
					for ( int y = startY; y <= endY; y = y + 1 )
					{
						for ( int z = startZ; z <= endZ; z = z + 1 )
						{

							int neighbour = x * (grid_.depthSize_ * grid_.heightSize_) + y * grid_.depthSize_ + z;

							if ( grid_.cells_.find( neighbour ) != grid_.cells_.end() )
							{
								//if ( neighbour != pair.first )
								//{
									for ( auto i : pair.second )
									{
										for ( auto j : grid_.cells_[neighbour] )
										{
											if ( i != j )
											{
		
												int index = IDX(i, j);
												if (!checkedPairs[index])
												{
													XMVECTOR distance = balls_[i].position_ - balls_[j].position_;

													if ( XMVectorGetX( XMVector3Length( distance ) ) < 1 )
													{
														CollisionResponse( &balls_[i], &balls_[j], distance );
													}
													checkedPairs[index] = true;
													pairs.push_back(index);
												}
											}
										}
									}
							}
						
						}
					}
				}
			}
			//std::cout << "\tCollision time: " << myTimer.update().time << std::endl;

		}

		//std::cout << "Time passed: " << myTimer.update().time << std::endl;
	}

	void Simulation::CollisionResponse(Ball* p1, Ball* p2, XMVECTOR distance)
	{
		float lambda = 90.0f;
		float force = lambda * (1.0f - XMVectorGetX(XMVector3Length(distance)));

		XMVECTOR forceVector = force * XMVector3Normalize(distance);

		p1->force_ = XMVectorAdd( p1->force_, forceVector );
		p2->force_ = XMVectorAdd( p2->force_, -forceVector );

		//printf( ": %f\n", force );

		//printf( "- Force: %f; Force vector: %f,%f,%f\n", force, XMVectorGetX( p2->force_ ), XMVectorGetY( p2->force_ ), XMVectorGetZ( p2->force_ ) );
	}
}