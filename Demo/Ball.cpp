#include "DXUT.h"
#include "Ball.h"

namespace AdvancedCollision
{
	Ball::Ball( XMVECTOR position, XMVECTOR velocity, XMVECTOR force, GeometricPrimitive* pSphere ) :
		position_( position ),
		velocity_( velocity ),
		force_( force ),
		pSphere_( pSphere )
	{}

	Ball::~Ball()
	{  }

	void Ball::ClearForce()
	{
		force_ = XMVectorZero();
	}

	// Perform Leap-frog step
	void Ball::Update( float fElapsedTime )
	{
		XMVECTOR totalForce = force_ -velocity_ * 0.5f;
		velocity_ = velocity_ + fElapsedTime * totalForce;
		position_ = position_ + fElapsedTime * velocity_;
	}

	void Ball::Destroy()
	{
		SAFE_DELETE( pSphere_ );
	}

	Ball& Ball::operator= (Ball a) {
		position_ = a.position_;
		velocity_ = a.velocity_;
		force_ = a.force_;
		pSphere_ = a.pSphere_;
		mass_ = a.mass_;
		radius_ = a.radius_;

		return *this;
	}
}