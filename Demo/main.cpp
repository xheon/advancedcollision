//--------------------------------------------------------------------------------------
// File: main.cpp
//
// The main file containing the entry point main().
//--------------------------------------------------------------------------------------

#include <sstream>
#include <iomanip>
#include <random>
#include <iostream>

//DirectX includes
#include <DirectXMath.h>
using namespace DirectX;

// Effect framework includes
#include <d3dx11effect.h>

// DXUT includes
#include <DXUT.h>
#include <DXUTcamera.h>

// DirectXTK includes
#include "Effects.h"
#include "VertexTypes.h"
#include "PrimitiveBatch.h"
#include "GeometricPrimitive.h"
#include "ScreenGrab.h"

// AntTweakBar includes
#include "AntTweakBar.h"

// Internal includes
#include "util/util.h"
#include "util/FFmpeg.h"

// Own includes
#include "Ball.h"
#include "Simulation.h"
#include "collisionDetect.h"
#include "timer.h"

// DXUT camera
// NOTE: CModelViewerCamera does not only manage the standard view transformation/camera position
//       (CModelViewerCamera::GetViewMatrix()), but also allows for model rotation
//       (CModelViewerCamera::GetWorldMatrix()).
//       Look out for CModelViewerCamera::SetButtonMasks(...).
CModelViewerCamera g_camera;

// Effect corresponding to "effect.fx"
ID3DX11Effect* g_pEffect = nullptr;

// Main tweak bar
TwBar* g_pTweakBar;

// DirectXTK effects, input layouts and primitive batches for different vertex types
BasicEffect*                               g_pEffectPositionColor = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionColor = nullptr;
PrimitiveBatch<VertexPositionColor>*       g_pPrimitiveBatchPositionColor = nullptr;

BasicEffect*                               g_pEffectPositionNormal = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionNormal = nullptr;
PrimitiveBatch<VertexPositionNormal>*      g_pPrimitiveBatchPositionNormal = nullptr;

BasicEffect*                               g_pEffectPositionNormalColor = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionNormalColor = nullptr;
PrimitiveBatch<VertexPositionNormalColor>* g_pPrimitiveBatchPositionNormalColor = nullptr;

// List all objects
std::vector<AdvancedCollision::Simulation> g_Simulations;

// Movable object management
XMINT2   g_viMouseDelta = XMINT2( 0, 0 );
XMVECTOR g_vfMovableObjectPos = XMVectorSet( 0, 0, 0, 0 );

// TweakAntBar GUI variables
float g_fGravity = -9.81f;

// Other global variables
float g_fDeltaTime = 1.0f / 128.0f;
float g_fTimeAcc = 0.0f;
float g_Time = 0.0f;
float g_fForce = 0.0f;
bool g_bNextStep = false;
int g_iTimeStep = 0;
int g_iSetup = 0;
int g_iOldSetup = 0;
int g_iPerfSetup = 0;
int g_iPerfOldSetup = 0;
int g_iAxis = -1;
bool g_bInteractionEnable = false;
MuTime g_myTime;
long g_lPassedTime[3];


// Video recorder
FFmpeg* g_pFFmpegVideoRecorder = nullptr;

// Function prototypes
void InitSimulation( ID3D11DeviceContext* pd3dImmediateContext );

// Create TweakBar and add required buttons and variables
void InitTweakBar( ID3D11Device* pd3dDevice )
{
	//std::string label = "label='" + std::to_string( g_iTimeStep ) + "'";

	TwInit( TW_DIRECT3D11, pd3dDevice );

	g_pTweakBar = TwNewBar( "TweakBar" );

	//ID3D11DeviceContext* pd3dImmediateContext;
	//pd3dDevice->GetImmediateContext(&pd3dImmediateContext);

	// HINT: For buttons you can directly pass the callback function as a lambda expression.
	TwAddButton( g_pTweakBar, "Reset Camera", []( void * )
	{
		g_camera.Reset();
	}, nullptr, "" );
	TwAddButton( g_pTweakBar, "Reset Simulations", []( void * context )
	{
		InitSimulation( (ID3D11DeviceContext*)context );
		printf( "Reset current setup\n" );
	}, DXUTGetD3D11DeviceContext(), "" );
	TwAddVarRW( g_pTweakBar, "Setup", TW_TYPE_INT16, &g_iSetup, "min=0 max=2" );
	TwAddButton( g_pTweakBar, "0: All three simulation", NULL, NULL, "" );
	TwAddButton( g_pTweakBar, "1: Tree (best result) with user interaction", NULL, NULL, "" );
	TwAddButton( g_pTweakBar, "2: Performance test", NULL, NULL, "" );
	TwAddVarRW( g_pTweakBar, "Performance setup", TW_TYPE_INT16, &g_iPerfSetup, "min=0 max=2" );
	TwAddButton( g_pTweakBar, "for setup 2 only", NULL, NULL, "label='0=100, 1=1000, 2=10000'" );
	TwAddVarRW( g_pTweakBar, "Gravity", TW_TYPE_FLOAT, &g_fGravity, "step=0.01" );
	//TwAddButton( g_pTweakBar, "Time step:", NULL, NULL,  label.c_str());
	TwAddVarRW( g_pTweakBar, "Time Step", TW_TYPE_INT32, &g_iTimeStep, "step=0" );
	TwAddVarRW(g_pTweakBar, "Axis", TW_TYPE_INT32, &g_iAxis, "step=1 min=-1 max=2");
	TwAddVarRW(g_pTweakBar, "Force", TW_TYPE_FLOAT, &g_fForce, "step=0.01");

}

void InitSimulation( ID3D11DeviceContext* pd3dImmediateContext )
{
	g_iTimeStep = 0;

	// Clear everything first
	for ( auto& it : g_Simulations )
	{
		it.CleanUp();
	}

	g_Simulations.clear();

	switch (g_iSetup)
	{
		case 0:
		{
			g_bInteractionEnable = false;
			g_Simulations.emplace_back(XMVectorSet(0, 0, 0, 0), 20.0f, 10, 5, 5, AdvancedCollision::NAIVE);
			g_Simulations.emplace_back(XMVectorSet(0, 0, 0, 0), 20.0f, 10, 5, 5, AdvancedCollision::KDTREE);
			g_Simulations.emplace_back(XMVectorSet(0, 0, 0, 0), 20.0f, 10, 5, 5, AdvancedCollision::GRID);

			for (auto& it : g_Simulations)
			{
				it.Initialize(pd3dImmediateContext);
			}
			break;
		}
		case 1:
		{
			g_bInteractionEnable = true;
			g_Simulations.emplace_back(XMVectorSet(0, 0, 0, 0), 30.0f, 3, 4, 7, AdvancedCollision::KDTREE);
			
			for (auto& it : g_Simulations)
			{
				it.Initialize(pd3dImmediateContext);
			}
			break;
		}
		case 2:
		{
			g_bInteractionEnable = false;
			
			int x, y, z;
			
			switch ( g_iPerfSetup )
			{
				case 0:
					x = 5;
					y = 5;
					z = 4;
					break;

				case 1:
					x = 25;
					y = 4;
					z = 10;
					break;

				case 2:
					x = 25;
					y = 40;
					z = 10;
					break;
			}
					
			g_Simulations.emplace_back( XMVectorSet( 0, 0, 0, 0 ), 60.0f, x,y,z, AdvancedCollision::NAIVE );
			g_Simulations.emplace_back( XMVectorSet( 0, 0, 0, 0 ), 60.0f, x,y,z, AdvancedCollision::KDTREE );
			g_Simulations.emplace_back( XMVectorSet( 0, 0, 0, 0 ), 60.0f, x,y,z, AdvancedCollision::GRID );

			for ( auto& it : g_Simulations )
			{
				it.Initialize( pd3dImmediateContext );
			}

			printf( "Performance test for 500 timesteps and %i balls: press <space> to start\n", x*y*z );
			g_bNextStep = false;
			//g_myTime.get();

			break;
		}
	}

	//g_myTime.get();

}

void DrawSimulations( ID3D11DeviceContext* pd3dImmediateContext )
{


	for ( auto& it : g_Simulations )
	{
		/*
		 Draw boxes first
		 */

		// Setup position/color effect
		g_pEffectPositionColor->SetWorld( g_camera.GetWorldMatrix() );

		g_pEffectPositionColor->Apply( pd3dImmediateContext );
		pd3dImmediateContext->IASetInputLayout( g_pInputLayoutPositionColor );

		// Draw
		g_pPrimitiveBatchPositionColor->Begin();

		// Lines in x direction (red color)
		for ( int i = 0; i < 4; i++ )
		{
			g_pPrimitiveBatchPositionColor->DrawLine(
				VertexPositionColor(
				XMVectorSet(
				XMVectorGetX( it.position_ ) - it.size_ / 2,
				XMVectorGetY( it.position_ ) + (float)(i % 2)*it.size_ - it.size_ / 2,
				XMVectorGetZ( it.position_ ) + (float)(i / 2)*it.size_ - it.size_ / 2, 1
				),
				Colors::Red ),
				VertexPositionColor(
				XMVectorSet(
				XMVectorGetX( it.position_ ) + it.size_ / 2,
				XMVectorGetY( it.position_ ) + (float)(i % 2)*it.size_ - it.size_ / 2,
				XMVectorGetZ( it.position_ ) + (float)(i / 2)*it.size_ - it.size_ / 2, 1
				),
				Colors::Red )
				);
		}

		// Lines in y direction
		for ( int i = 0; i < 4; i++ )
		{
			g_pPrimitiveBatchPositionColor->DrawLine(
				VertexPositionColor(
				XMVectorSet(
				XMVectorGetX( it.position_ ) + (float)(i % 2)*it.size_ - it.size_ / 2,
				XMVectorGetY( it.position_ ) - it.size_ / 2,
				XMVectorGetZ( it.position_ ) + (float)(i / 2)*it.size_ - it.size_ / 2, 1
				),
				Colors::Green ),
				VertexPositionColor(
				XMVectorSet(
				XMVectorGetX( it.position_ ) + (float)(i % 2)*it.size_ - it.size_ / 2,
				XMVectorGetY( it.position_ ) + it.size_ / 2,
				XMVectorGetZ( it.position_ ) + (float)(i / 2)*it.size_ - it.size_ / 2, 1
				),
				Colors::Green )
				);
		}

		// Lines in z direction
		for ( int i = 0; i < 4; i++ )
		{
			g_pPrimitiveBatchPositionColor->DrawLine(
				VertexPositionColor(
				XMVectorSet(
				XMVectorGetX( it.position_ ) + (float)(i % 2)*it.size_ - it.size_ / 2,
				XMVectorGetY( it.position_ ) + (float)(i / 2)*it.size_ - it.size_ / 2,
				XMVectorGetZ( it.position_ ) - it.size_ / 2, 1
				),
				Colors::Blue ),
				VertexPositionColor(
				XMVectorSet(
				XMVectorGetX( it.position_ ) + (float)(i % 2)*it.size_ - it.size_ / 2,
				XMVectorGetY( it.position_ ) + (float)(i / 2)*it.size_ - it.size_ / 2,
				XMVectorGetZ( it.position_ ) + it.size_ / 2, 1
				),
				Colors::Blue )
				);
		}

		g_pPrimitiveBatchPositionColor->End();

		/*
		 Draw balls
		 */
		for ( auto& jt : it.balls_ )
		{
			switch ( it.storage_ )
			{
				case AdvancedCollision::NAIVE:
					g_pEffectPositionNormal->SetDiffuseColor( 0.6f *  XMVectorSet( 0, 1, 1, 0 ) );
					break;
				case AdvancedCollision::KDTREE:
					g_pEffectPositionNormal->SetDiffuseColor( 0.6f *  XMVectorSet( 1, 0, 0, 0 ) );
					break;
				case AdvancedCollision::GRID:
					g_pEffectPositionNormal->SetDiffuseColor( 0.6f *  XMVectorSet( 1, 1, 0, 0 ) );
					break;
				default:
					g_pEffectPositionNormal->SetDiffuseColor( 0.6f *  XMVectorSet( 1, 1, 1, 0 ) );
					break;
			}

			XMMATRIX trans = XMMatrixTranslationFromVector( it.position_ + jt.position_ );
			g_pEffectPositionNormal->SetWorld( trans * g_camera.GetWorldMatrix() );

			jt.pSphere_->Draw( g_pEffectPositionNormal, g_pInputLayoutPositionNormal );
		}
	}
}

// Draw a large, square plane at y=-1 with a checkerboard pattern
// (Drawn as multiple quads, i.e. triangle strips, using a DirectXTK primitive batch)
void DrawFloor( ID3D11DeviceContext* pd3dImmediateContext )
{
	// Setup position/normal/color effect
	g_pEffectPositionNormalColor->SetWorld( XMMatrixIdentity() );
	g_pEffectPositionNormalColor->SetEmissiveColor( Colors::Black );
	g_pEffectPositionNormalColor->SetDiffuseColor( 0.8f * Colors::White );
	g_pEffectPositionNormalColor->SetSpecularColor( 0.4f * Colors::White );
	g_pEffectPositionNormalColor->SetSpecularPower( 1000 );

	g_pEffectPositionNormalColor->Apply( pd3dImmediateContext );
	pd3dImmediateContext->IASetInputLayout( g_pInputLayoutPositionNormalColor );

	// Draw 4*n*n quads spanning x = [-n;n], y = -1, z = [-n;n]
	const float n = 4;
	XMVECTOR normal = XMVectorSet( 0, 1, 0, 0 );
	XMVECTOR planecenter = XMVectorSet( 0, -0.5f, 0, 0 );

	g_pPrimitiveBatchPositionNormalColor->Begin();
	for ( float z = -n; z < n; z++ )
	{
		for ( float x = -n; x < n; x++ )
		{
			// Quad vertex positions
			XMVECTOR pos[] = {XMVectorSet( x, -0.5f, z + 1, 0 ),
				XMVectorSet( x + 1, -0.5f, z + 1, 0 ),
				XMVectorSet( x + 1, -0.5f, z, 0 ),
				XMVectorSet( x, -0.5f, z, 0 )};

			// Color checkerboard pattern (white & gray)
			XMVECTOR color = ((int( z + x ) % 2) == 0) ? XMVectorSet( 1, 1, 1, 1 ) : XMVectorSet( 0.6f, 0.6f, 0.6f, 1 );

			// Color attenuation based on distance to plane center
			float attenuation[] = {
				1.0f - XMVectorGetX( XMVector3Length( pos[0] - planecenter ) ) / n,
				1.0f - XMVectorGetX( XMVector3Length( pos[1] - planecenter ) ) / n,
				1.0f - XMVectorGetX( XMVector3Length( pos[2] - planecenter ) ) / n,
				1.0f - XMVectorGetX( XMVector3Length( pos[3] - planecenter ) ) / n};

			g_pPrimitiveBatchPositionNormalColor->DrawQuad(
				VertexPositionNormalColor( pos[0], normal, attenuation[0] * color ),
				VertexPositionNormalColor( pos[1], normal, attenuation[1] * color ),
				VertexPositionNormalColor( pos[2], normal, attenuation[2] * color ),
				VertexPositionNormalColor( pos[3], normal, attenuation[3] * color )
				);
		}
	}
	g_pPrimitiveBatchPositionNormalColor->End();
}

// ============================================================
// DXUT Callbacks
// ============================================================

//--------------------------------------------------------------------------------------
// Reject any D3D11 devices that aren't acceptable by returning false
//--------------------------------------------------------------------------------------
bool CALLBACK IsD3D11DeviceAcceptable( const CD3D11EnumAdapterInfo *AdapterInfo, UINT Output, const CD3D11EnumDeviceInfo *DeviceInfo,
									   DXGI_FORMAT BackBufferFormat, bool bWindowed, void* pUserContext )
{
	return true;
}

//--------------------------------------------------------------------------------------
// Called right before creating a device, allowing the app to modify the device settings as needed
//--------------------------------------------------------------------------------------
bool CALLBACK ModifyDeviceSettings( DXUTDeviceSettings* pDeviceSettings, void* pUserContext )
{
	return true;
}

//--------------------------------------------------------------------------------------
// Create any D3D11 resources that aren't dependent on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11CreateDevice( ID3D11Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
	HRESULT hr;

	ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();;

	std::wcout << L"Device: " << DXUTGetDeviceStats() << std::endl;

	// Load custom effect from "effect.fxo" (compiled "effect.fx")
	std::wstring effectPath = GetExePath() + L"effect.fxo";
	if ( FAILED( hr = D3DX11CreateEffectFromFile( effectPath.c_str(), 0, pd3dDevice, &g_pEffect ) ) )
	{
		std::wcout << L"Failed creating effect with error code " << int( hr ) << std::endl;
		return hr;
	}

	InitSimulation( pd3dImmediateContext );

	// Init AntTweakBar GUI
	InitTweakBar( pd3dDevice );

	// Create DirectXTK geometric primitives for later usage

	// Create effect, input layout and primitive batch for position/color vertices (DirectXTK)
	{
		// Effect
		g_pEffectPositionColor = new BasicEffect( pd3dDevice );
		g_pEffectPositionColor->SetVertexColorEnabled( true ); // triggers usage of position/color vertices

		// Input layout
		void const* shaderByteCode;
		size_t byteCodeLength;
		g_pEffectPositionColor->GetVertexShaderBytecode( &shaderByteCode, &byteCodeLength );

		pd3dDevice->CreateInputLayout( VertexPositionColor::InputElements,
									   VertexPositionColor::InputElementCount,
									   shaderByteCode, byteCodeLength,
									   &g_pInputLayoutPositionColor );

		// Primitive batch
		g_pPrimitiveBatchPositionColor = new PrimitiveBatch<VertexPositionColor>( pd3dImmediateContext );
	}

	// Create effect, input layout and primitive batch for position/normal vertices (DirectXTK)
	{
		// Effect
		g_pEffectPositionNormal = new BasicEffect( pd3dDevice );
		g_pEffectPositionNormal->EnableDefaultLighting(); // triggers usage of position/normal vertices
		g_pEffectPositionNormal->SetPerPixelLighting( true );

		// Input layout
		void const* shaderByteCode;
		size_t byteCodeLength;
		g_pEffectPositionNormal->GetVertexShaderBytecode( &shaderByteCode, &byteCodeLength );

		pd3dDevice->CreateInputLayout( VertexPositionNormal::InputElements,
									   VertexPositionNormal::InputElementCount,
									   shaderByteCode, byteCodeLength,
									   &g_pInputLayoutPositionNormal );

		// Primitive batch
		g_pPrimitiveBatchPositionNormal = new PrimitiveBatch<VertexPositionNormal>( pd3dImmediateContext );
	}

	// Create effect, input layout and primitive batch for position/normal/color vertices (DirectXTK)
	{
		// Effect
		g_pEffectPositionNormalColor = new BasicEffect( pd3dDevice );
		g_pEffectPositionNormalColor->SetPerPixelLighting( true );
		g_pEffectPositionNormalColor->EnableDefaultLighting();     // triggers usage of position/normal/color vertices
		g_pEffectPositionNormalColor->SetVertexColorEnabled( true ); // triggers usage of position/normal/color vertices

		// Input layout
		void const* shaderByteCode;
		size_t byteCodeLength;
		g_pEffectPositionNormalColor->GetVertexShaderBytecode( &shaderByteCode, &byteCodeLength );

		pd3dDevice->CreateInputLayout( VertexPositionNormalColor::InputElements,
									   VertexPositionNormalColor::InputElementCount,
									   shaderByteCode, byteCodeLength,
									   &g_pInputLayoutPositionNormalColor );

		// Primitive batch
		g_pPrimitiveBatchPositionNormalColor = new PrimitiveBatch<VertexPositionNormalColor>( pd3dImmediateContext );
	}

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11CreateDevice
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11DestroyDevice( void* pUserContext )
{
	SAFE_RELEASE( g_pEffect );

	TwDeleteBar( g_pTweakBar );
	g_pTweakBar = nullptr;
	TwTerminate();

	SAFE_DELETE( g_pPrimitiveBatchPositionColor );
	SAFE_RELEASE( g_pInputLayoutPositionColor );
	SAFE_DELETE( g_pEffectPositionColor );

	SAFE_DELETE( g_pPrimitiveBatchPositionNormal );
	SAFE_RELEASE( g_pInputLayoutPositionNormal );
	SAFE_DELETE( g_pEffectPositionNormal );

	SAFE_DELETE( g_pPrimitiveBatchPositionNormalColor );
	SAFE_RELEASE( g_pInputLayoutPositionNormalColor );
	SAFE_DELETE( g_pEffectPositionNormalColor );
}

//--------------------------------------------------------------------------------------
// Create any D3D11 resources that depend on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11ResizedSwapChain( ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
										  const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
	// Update camera parameters
	int width = pBackBufferSurfaceDesc->Width;
	int height = pBackBufferSurfaceDesc->Height;
	g_camera.SetWindow( width, height );
	g_camera.SetProjParams( XM_PI / 4.0f, float( width ) / float( height ), 0.1f, 100.0f );

	// Inform AntTweakBar about back buffer resolution change
	TwWindowSize( pBackBufferSurfaceDesc->Width, pBackBufferSurfaceDesc->Height );

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11ResizedSwapChain
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11ReleasingSwapChain( void* pUserContext )
{}

//--------------------------------------------------------------------------------------
// Handle key presses
//--------------------------------------------------------------------------------------
void CALLBACK OnKeyboard( UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext )
{
	HRESULT hr;

	if ( bKeyDown )
	{
		switch ( nChar )
		{
			// RETURN: toggle fullscreen
			case VK_RETURN:
			{
				if ( bAltDown ) DXUTToggleFullScreen();
				break;
			}
			// F8: Take screenshot
			case VK_F8:
			{
				// Save current render target as png
				static int nr = 0;
				std::wstringstream ss;
				ss << L"Screenshot" << std::setfill( L'0' ) << std::setw( 4 ) << nr++ << L".png";

				ID3D11Resource* pTex2D = nullptr;
				DXUTGetD3D11RenderTargetView()->GetResource( &pTex2D );
				SaveWICTextureToFile( DXUTGetD3D11DeviceContext(), pTex2D, GUID_ContainerFormatPng, ss.str().c_str() );
				SAFE_RELEASE( pTex2D );

				std::wcout << L"Screenshot written to " << ss.str() << std::endl;
				break;
			}
			// F10: Toggle video recording
			case VK_F10:
			{
				if ( !g_pFFmpegVideoRecorder )
				{
					g_pFFmpegVideoRecorder = new FFmpeg( 25, 21, FFmpeg::MODE_INTERPOLATE );
					V( g_pFFmpegVideoRecorder->StartRecording( DXUTGetD3D11Device(), DXUTGetD3D11RenderTargetView(), "output.avi" ) );
				}
				else
				{
					g_pFFmpegVideoRecorder->StopRecording();
					SAFE_DELETE( g_pFFmpegVideoRecorder );
				}
			}
			// Space next step
			case VK_SPACE:
				g_bNextStep = !g_bNextStep;
		}
	}
}

//--------------------------------------------------------------------------------------
// Handle mouse button presses
//--------------------------------------------------------------------------------------
void CALLBACK OnMouse( bool bLeftButtonDown, bool bRightButtonDown, bool bMiddleButtonDown,
					   bool bSideButton1Down, bool bSideButton2Down, int nMouseWheelDelta,
					   int xPos, int yPos, void* pUserContext )
{
	// Track mouse movement if left mouse key is pressed
	{
		static int xPosSave = 0, yPosSave = 0;

		if ( bLeftButtonDown )
		{
			// Accumulate deltas in g_viMouseDelta
			g_viMouseDelta.x += xPos - xPosSave;
			g_viMouseDelta.y += yPos - yPosSave;
		}

		xPosSave = xPos;
		yPosSave = yPos;
	}
}

//--------------------------------------------------------------------------------------
// Handle messages to the application
//--------------------------------------------------------------------------------------
LRESULT CALLBACK MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam,
						  bool* pbNoFurtherProcessing, void* pUserContext )
{
	// Send message to AntTweakbar first
	if ( TwEventWin( hWnd, uMsg, wParam, lParam ) )
	{
		*pbNoFurtherProcessing = true;
		return 0;
	}

	// If message not processed yet, send to camera
	if ( g_camera.HandleMessages( hWnd, uMsg, wParam, lParam ) )
	{
		*pbNoFurtherProcessing = true;
		return 0;
	}

	return 0;
}

//--------------------------------------------------------------------------------------
// Handle updates to the scene
//--------------------------------------------------------------------------------------
void CALLBACK OnFrameMove( double dTime, float fElapsedTime, void* pUserContext )
{
	std::wstring title = g_bNextStep ? L"RUNNING" : L"PAUSED";

	UpdateWindowTitle( L"Advanced Collision Simulation -" + title + L"- " );

	// Move camera
	g_camera.FrameMove( fElapsedTime );

	g_fTimeAcc += fElapsedTime;


	while ( g_fTimeAcc > g_fDeltaTime && g_iTimeStep <= 500)
	{
		if (g_iOldSetup != g_iSetup)
		{
			g_iOldSetup = g_iSetup;
			InitSimulation(DXUTGetD3D11DeviceContext());
			TwTerminate();
			InitTweakBar(DXUTGetD3D11Device());
		}
		
		if ( g_iSetup == 2 )
		{
			if ( g_iPerfOldSetup != g_iPerfSetup )
			{
				g_iPerfOldSetup = g_iPerfSetup;
				InitSimulation( DXUTGetD3D11DeviceContext() );
				TwTerminate();
				InitTweakBar( DXUTGetD3D11Device() );
			}
		}

		if ( g_bNextStep == true )
		{
			for ( auto& it : g_Simulations )
			{
				switch ( it.storage_ )
				{
					/*case AdvancedCollision::NAIVE:
						printf( "NAIVE:\t" );
						break;
					case AdvancedCollision::KDTREE:
						printf( "TREE:\t" );
						break;
					case AdvancedCollision::GRID:
						printf( "GRID:\t" );
						break;*/
					
				}

				g_myTime.get();

				if (g_bInteractionEnable) {
					it.Update(g_fDeltaTime, g_iAxis, g_fForce);
				}
				else {
					it.Update(g_fDeltaTime);
				}

				switch ( it.storage_ )
				{
					case AdvancedCollision::NAIVE:
						g_lPassedTime[0] += g_myTime.update().time;
					break;
					case AdvancedCollision::KDTREE:
						g_lPassedTime[1] += g_myTime.update().time;
					break;
					case AdvancedCollision::GRID:
						g_lPassedTime[2] += g_myTime.update().time;
					break;

				}
				

			}
			g_iTimeStep++;
			//g_bNextStep = false;
			//printf( "--\n");
		}
		g_Time += g_fDeltaTime;
		g_fTimeAcc -= g_fDeltaTime;
	}

	if ( g_iTimeStep >= 500 && g_iSetup != 2 )
	{
		//g_bNextStep = false;
		g_iTimeStep = 0;
	}

	if ( g_iTimeStep >= 500 && g_iSetup == 2 && g_bNextStep == true )
	{
		//g_iTimeStep = 0;
		g_bNextStep = false;

		std::cout << "Naive: Time passed: " << g_lPassedTime[0] << std::endl;
		std::cout << "Tree: Time passed: " << g_lPassedTime[1] << std::endl;
		std::cout << "Grid: Time passed: " << g_lPassedTime[2] << std::endl;
	}//

	// Update effects with new view + proj transformations
	g_pEffectPositionColor->SetView( g_camera.GetViewMatrix() );
	g_pEffectPositionColor->SetProjection( g_camera.GetProjMatrix() );

	g_pEffectPositionNormal->SetView( g_camera.GetViewMatrix() );
	g_pEffectPositionNormal->SetProjection( g_camera.GetProjMatrix() );

	g_pEffectPositionNormalColor->SetView( g_camera.GetViewMatrix() );
	g_pEffectPositionNormalColor->SetProjection( g_camera.GetProjMatrix() );

	// Apply accumulated mouse deltas to g_vfMovableObjectPos (move along cameras view plane)
	if ( g_viMouseDelta.x != 0 || g_viMouseDelta.y != 0 )
	{
		// Calcuate camera directions in world space
		XMMATRIX viewInv = XMMatrixInverse( nullptr, g_camera.GetViewMatrix() );
		XMVECTOR camRightWorld = XMVector3TransformNormal( g_XMIdentityR0, viewInv );
		XMVECTOR camUpWorld = XMVector3TransformNormal( g_XMIdentityR1, viewInv );

		// Add accumulated mouse deltas to movable object pos
		XMVECTOR vMovableObjectPos; // = XMLoadFloat3( &g_vfMovableObjectPos );

		float speedScale = 1.0f;

		g_vfMovableObjectPos = XMVectorSet(0, 0, 0, 0);
		g_vfMovableObjectPos = XMVectorAdd(vMovableObjectPos, speedScale * (float)g_viMouseDelta.x * camRightWorld);
		g_vfMovableObjectPos = XMVectorAdd(vMovableObjectPos, -speedScale * (float)g_viMouseDelta.y * camUpWorld);

		// Reset accumulated mouse deltas
		g_viMouseDelta = XMINT2( 0, 0 );
	}
}

//--------------------------------------------------------------------------------------
// Render the scene using the D3D11 device
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11FrameRender( ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext,
								  double fTime, float fElapsedTime, void* pUserContext )
{
	HRESULT hr;

	// Clear render target and depth stencil
	float ClearColor[4] = {0.0f, 0.0f, 0.0f, 0.0f};

	ID3D11RenderTargetView* pRTV = DXUTGetD3D11RenderTargetView();
	ID3D11DepthStencilView* pDSV = DXUTGetD3D11DepthStencilView();
	pd3dImmediateContext->ClearRenderTargetView( pRTV, ClearColor );
	pd3dImmediateContext->ClearDepthStencilView( pDSV, D3D11_CLEAR_DEPTH, 1.0f, 0 );

	// Draw floor
	//DrawFloor(pd3dImmediateContext);

	if ( g_iSetup != 2 )
	{
		DrawSimulations( pd3dImmediateContext );
	}

	// Draw GUI
	TwDraw();

	if ( g_pFFmpegVideoRecorder )
	{
		V( g_pFFmpegVideoRecorder->AddFrame( pd3dImmediateContext, DXUTGetD3D11RenderTargetView() ) );
	}
}

//--------------------------------------------------------------------------------------
// Initialize everything and go into a render loop
//--------------------------------------------------------------------------------------
int main( int argc, char* argv[] )
{
#if defined(DEBUG) | defined(_DEBUG)
	// Enable run-time memory check for debug builds.
	// (on program exit, memory leaks are printed to Visual Studio's Output console)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

#ifdef _DEBUG
	std::wcout << L"---- DEBUG BUILD ----\n\n";
#endif

	// Set general DXUT callbacks
	DXUTSetCallbackMsgProc( MsgProc );
	DXUTSetCallbackMouse( OnMouse, true );
	DXUTSetCallbackKeyboard( OnKeyboard );

	DXUTSetCallbackFrameMove( OnFrameMove );
	DXUTSetCallbackDeviceChanging( ModifyDeviceSettings );

	// Set the D3D11 DXUT callbacks
	DXUTSetCallbackD3D11DeviceAcceptable( IsD3D11DeviceAcceptable );
	DXUTSetCallbackD3D11DeviceCreated( OnD3D11CreateDevice );
	DXUTSetCallbackD3D11SwapChainResized( OnD3D11ResizedSwapChain );
	DXUTSetCallbackD3D11FrameRender( OnD3D11FrameRender );
	DXUTSetCallbackD3D11SwapChainReleasing( OnD3D11ReleasingSwapChain );
	DXUTSetCallbackD3D11DeviceDestroyed( OnD3D11DestroyDevice );

	// Init camera
	XMFLOAT3 eye( 0.0f, 3.0f, -6.0f );
	XMFLOAT3 lookAt( 0.0f, 2.0f, 0.0f );
	g_camera.SetViewParams( XMLoadFloat3( &eye ), XMLoadFloat3( &lookAt ) );
	g_camera.SetButtonMasks( MOUSE_MIDDLE_BUTTON, MOUSE_WHEEL, MOUSE_RIGHT_BUTTON );

	// Init DXUT and create device
	DXUTInit( true, true, NULL ); // Parse the command line, show msgboxes on error, no extra command line params
	//DXUTSetIsInGammaCorrectMode( false ); // true by default (SRGB backbuffer), disable to force a RGB backbuffer
	DXUTSetCursorSettings( true, true ); // Show the cursor and clip it when in full screen
	DXUTCreateWindow( L"Advanced Collision Simulation" );
	DXUTCreateDevice( D3D_FEATURE_LEVEL_10_0, true, 800, 600 );

	DXUTMainLoop(); // Enter into the DXUT render loop

	DXUTShutdown(); // Shuts down DXUT (includes calls to OnD3D11ReleasingSwapChain() and OnD3D11DestroyDevice())

	return DXUTGetExitCode();
}