#include "Grid.h"
#include <math.h>

namespace AdvancedCollision
{
	void Grid::SortBalls( std::vector<Ball>& balls, float width, float height, float depth )
	{
		width_ = width;
		height_ = height;
		depth_ = depth;

		widthSize_ = static_cast<int> (ceil(width / h));
		heightSize_ = static_cast<int> (ceil(height / h));
		depthSize_ = static_cast<int> (ceil(depth / h));

		float normalizeW = width_ / 2;
		float normalizeH = height_ / 2;
		float normalizeD = depth_ / 2;
		
		int i = 0;
		for (const auto& ball : balls) {
			XMVECTOR normalizePosition = XMVectorSet(
				XMVectorGetX(ball.position_) + normalizeW,
				XMVectorGetY(ball.position_) + normalizeH,
				XMVectorGetZ(ball.position_) + normalizeD,
				0);

			int x = static_cast<int> (floor(XMVectorGetX(normalizePosition) / h));
			int y = static_cast<int> (floor(XMVectorGetY(normalizePosition) / h));
			int z = static_cast<int> (floor(XMVectorGetZ(normalizePosition) / h));

			int index = x * (heightSize_ * depthSize_) + y * (depthSize_) + z;

			if (cells_.find (index) == cells_.end())
			{
				cells_[index] = std::vector<int> ();
			}

			cells_[index].emplace_back( i );
			++i;
		}
	}
}